package db_1;

import javax.persistence.*;

/**
 * Created by Arek on 2014-11-09.
 */
@Entity(name = "recipe")
public class Recipe extends Restaurant {

    @Id
    private int recipeId;
    private int mealId;
    private int foodId;
    private float foodAmount;

    public Recipe() {
    }

    public Recipe(int recipeId, int mealId, int foodId, float foodAmount) {
        this.recipeId = recipeId;
        this.mealId = mealId;
        this.foodId = foodId;
        this.foodAmount = foodAmount;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public int getMealId() {
        return mealId;
    }

    public void setMealId(int mealId) {
        this.mealId = mealId;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public float getFoodAmount() {
        return foodAmount;
    }

    public void setFoodAmount(float foodAmount) {
        this.foodAmount = foodAmount;
    }

    @Override
    public String toString() {
        return "Recipe{"
                + "recipeId=" + recipeId
                + ", mealId=" + mealId
                + ", foodId=" + foodId
                + ", foodAmount=" + foodAmount
                + '}';
    }
}
