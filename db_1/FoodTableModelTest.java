package db_1;

import static org.junit.Assert.*;

import org.junit.Test;

public class FoodTableModelTest {

	@Test
	public void testIsCellEditable() {
		FoodTableModel test = new FoodTableModel();
		test.isCellEditable(5, 3);
	}

	@Test
	public void testFoodTableModel() {
		FoodTableModel test = new FoodTableModel();
	}

	@Test
	public void testGetRowCount() {
		FoodTableModel test = new FoodTableModel();
		test.getRowCount();
	}

	@Test
	public void testGetColumnCount() {
		FoodTableModel test = new FoodTableModel();
		test.getColumnCount();
	}

	@Test
	public void testGetColumnNameInt() {
		FoodTableModel test = new FoodTableModel();
		test.getColumnName(5);
	}

	@Test
	public void testGetColumnClassInt() {
		FoodTableModel test = new FoodTableModel();
		test.getColumnClass(5);
	}

	@Test
	public void testGetValueAt() {
		FoodTableModel test = new FoodTableModel();
		test.getValueAt(2,2);
	}

	@Test
	public void testSetValueAtObjectIntInt() {
		FoodTableModel test = new FoodTableModel();
		test.setValueAt(2, 10, 5);
	}

}
