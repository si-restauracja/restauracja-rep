/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db_1;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Arek
 */
public class AdminLogin extends JFrame implements ActionListener {

    JLabel lbName, lbPass;
    JTextField jtName;
    JPasswordField jpPass;
    JButton bConfirm;
    Connection connection = new Connection();
    ArrayList<Admins> adminsList;

    public AdminLogin() {
        setLayout(null);
        setTitle("Zaloguj");
        setSize(400, 200);
        setLocation(200, 200);
        setVisible(true);

        lbName = new JLabel("Nazwisko:");
        lbName.setFont(new Font("Sas-serif", Font.BOLD, 13));
        lbName.setSize(100, 20);
        lbName.setLocation(10, 10);
        add(lbName);
        lbPass = new JLabel("Hasło");
        lbPass.setFont(new Font("Sans-serif", Font.BOLD, 13));
        lbPass.setSize(100, 20);
        lbPass.setLocation(10, 40);
        add(lbPass);
        jtName = new JTextField();
        jtName.setLocation(100, 10);
        jtName.setSize(150, 20);
        add(jtName);
        jpPass = new JPasswordField();
        jpPass.setLocation(100, 40);
        jpPass.setSize(150, 20);
        add(jpPass);
        bConfirm = new JButton("OK");
        bConfirm.setLocation(300, 20);
        bConfirm.setSize(60, 30);
        bConfirm.addActionListener(this);
        add(bConfirm);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == bConfirm) {
            connection.createSession();
            connection.getRecords();
            adminsList = new ArrayList<Admins>(connection.adminsList);
            Iterator<Admins> adminsIterator = adminsList.iterator();

            while (adminsIterator.hasNext()) {
                Admins temp = adminsIterator.next();
                System.out.println(temp.toString());
                String str1 = temp.getAdminName();
                String str2 = jtName.getText();
                String str3 = temp.getAdminPassword();
                String str4 = String.valueOf(jpPass.getPassword());
                if (str1.equals(str2) & str3.equals(str4)) {
                    AssistantFrame pomocnikFrame = new AssistantFrame();
                    CookFrame kucharzFrame = new CookFrame();
                    dispose();
                    break;
                } else {
                    JOptionPane.showMessageDialog(null, "Nie udało się zalogować");
                    break;
                }
            }
        }
    }
}
