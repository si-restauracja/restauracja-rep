package db_1;

import static org.junit.Assert.*;

import org.junit.Test;

public class FoodTest {

	@Test
	public void testFood() {
		Food test = new Food();
	}

	@Test
	public void testFoodIntStringFloat() {
		Food test = new Food(5,"Pomidor",5.0f);
	}

	@Test
	public void testGetFoodId() {
		Food test = new Food(5,"Pomidor",5.0f);
		test.getFoodId();
	}

	@Test
	public void testSetFoodId() {
		Food test = new Food();
		test.setFoodId(5);
	}

	@Test
	public void testGetFoodName() {
		Food test = new Food(5,"Pomidor",5.0f);
		test.getFoodName();
	}

	@Test
	public void testSetFoodName() {
		Food test = new Food();
		test.setFoodName("pomidor");
	}

	@Test
	public void testGetFoodAmount() {
		Food test = new Food(5,"Pomidor",5.0f);
		test.getFoodAmount();
	}

	@Test
	public void testSetFoodAmount() {
		Food test = new Food();
		test.setFoodAmount(5.0f);
	}

	@Test
	public void testToString() {
		Food test = new Food(5,"Pomidor",5.0f);
		test.toString();
	}

}
