package db_1;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;

public class DanieDniaTableModel extends AbstractTableModel {

    ArrayList<Meal> chosenMeals;
    String[] columnNames = new String[]{"ID", "Nazwa", "Ilość"};

    public DanieDniaTableModel(ArrayList<Meal> meals) {
        chosenMeals = meals;
    }

    public int getRowCount() {
        return chosenMeals.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Meal meal = chosenMeals.get(rowIndex);
        Object[] values = new Object[]{meal.getMealId(), meal.getMealName(), meal.getMealAmount()};
        return values[columnIndex];
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void updateMeal(int id) {
        Connection connection = new Connection();
        connection.createSession();
        connection.getRecords();
        ArrayList<Meal> meals = new ArrayList<Meal>(connection.mealList);
        for (int i = 0; i < chosenMeals.size(); i++) {
            if (chosenMeals.get(i).getMealId() == id) {
                for (Meal meal : meals) {
                    if (meal.getMealId() == id) {
                        chosenMeals.set(i, meal);
                    }
                }
            }
        }
        fireTableDataChanged();

    }
}
