package db_1;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class AssistantFrame extends SuperFrame implements ActionListener {

    JButton bConfirm, bClear;
    JLabel lbInfo, lbName, lbAmount;
    JTextField jtName, jtAmount;
    JTable table;

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == miExit) {
            dispose();
        } else if (source == miCook) {
            CookLogin cookLogin = new CookLogin();
        } else if (source == miAssistant) {
            AssistantLogin assistantLogin = new AssistantLogin();
        } else if (source == miAdmin) {
            AdminLogin adminLogin = new AdminLogin();
        } else if (source == bConfirm) {
            int i = table.getRowCount() + 1;
            String name = jtName.getText();
            float amount = Float.parseFloat(jtAmount.getText());
            Food food = new Food(i, name, amount);
            Connection o = new Connection();
            o.createSession();
            o.updateRecord(food);
            dispose();
            AssistantFrame pomocnikFrame = new AssistantFrame();
            pomocnikFrame.pack();
            pomocnikFrame.setTitle("Pomocnik");
            pomocnikFrame.setVisible(true);
        } else if (source == bClear) {
            jtName.setText(null);
            jtAmount.setText(null);
        } else if (source == miHelp) {
            AuthorsFrame authorsFrame = new AuthorsFrame();
        }
    }

    public AssistantFrame() {
        initMenu();

        table = new JTable(new FoodTableModel());
        table.setRowHeight(14);
        int rowHeight = table.getRowHeight();
        int rowCount = table.getRowCount();

        setPreferredSize(new Dimension(400, rowHeight * rowCount + 180));
        pack();
        setTitle("Pomocnik");
        setVisible(true);

        lbInfo = new JLabel("Dodaj nowy produkt:");
        lbInfo.setFont(new Font("Sans-serif", Font.BOLD, 13));
        lbInfo.setSize(250, 20);
        lbInfo.setLocation(10, rowHeight * rowCount + 20);
        add(lbInfo);

        lbName = new JLabel("Nazwa:");
        lbName.setFont(new Font("Sans-serif", Font.BOLD, 13));
        lbName.setSize(100, 20);
        lbName.setLocation(10, rowHeight * rowCount + 50);
        add(lbName);
        lbAmount = new JLabel("Ilość:");
        lbAmount.setFont(new Font("Sans-serif", Font.BOLD, 13));
        lbAmount.setSize(100, 20);
        lbAmount.setLocation(10, rowHeight * rowCount + 80);
        add(lbAmount);
        jtName = new JTextField();
        jtName.setLocation(100, rowHeight * rowCount + 50);
        jtName.setSize(150, 20);
        add(jtName);
        jtAmount = new JTextField();
        jtAmount.setLocation(100, rowHeight * rowCount + 80);
        jtAmount.setSize(150, 20);
        add(jtAmount);
        bConfirm = new JButton("Dodaj");
        bConfirm.setLocation(280, rowHeight * rowCount + 50);
        bConfirm.setSize(90, 20);
        add(bConfirm);
        bConfirm.addActionListener(this);

        bClear = new JButton("Wyczyść");
        bClear.setLocation(280, rowHeight * rowCount + 80);
        bClear.setSize(90, 20);
        add(bClear);
        bClear.addActionListener(this);

        JScrollPane scrollPane = new JScrollPane(table);
        //table.setFillsViewportHeight(true);
        table.setAutoCreateRowSorter(true);
        setWidthAsPercentages(table, 0.05, 0.70, 0.25);
        add(scrollPane, BorderLayout.CENTER);

    }

    private static void setWidthAsPercentages(JTable table,
            double... percentages) {
        final double factor = 10000;

        TableColumnModel model = table.getColumnModel();
        for (int columnIndex = 0; columnIndex < percentages.length; columnIndex++) {
            TableColumn column = model.getColumn(columnIndex);
            column.setPreferredWidth((int) (percentages[columnIndex] * factor));
        }
    }
}
