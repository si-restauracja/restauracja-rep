package db_1;

import static org.junit.Assert.*;

import org.junit.Test;

public class MealTest {

	@Test
	public void testMeal() {
		Meal test = new Meal();
	}

	@Test
	public void testMealIntStringFloat() {
		Meal test = new Meal(5,"Jajecznica", 5.4f);
	}

	@Test
	public void testGetMealId() {
		Meal test = new Meal(5,"Jajecznica", 5.4f);
		test.getMealId();
	}

	@Test
	public void testSetMealId() {
		Meal test = new Meal();
		test.setMealId(5);
	}

	@Test
	public void testGetMealName() {
		Meal test = new Meal(5,"Jajecznica", 5.4f);
		test.getMealName();
	}

	@Test
	public void testSetMealName() {
		Meal test = new Meal();
		test.setMealName("Jajecznica");
	}

	@Test
	public void testGetMealAmount() {
		Meal test = new Meal(5,"Jajecznica", 5.4f);
		test.getMealAmount();
	}

	@Test
	public void testSetMealAmount() {
		Meal test = new Meal();
		test.setMealAmount(5.2f);
	}

	@Test
	public void testToString() {
		Meal test = new Meal(5,"Jajecznica", 5.4f);
		test.toString();
	}

}
