package db_1;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Arek on 2014-11-09.
 */
@Entity(name = "meal")
public class Meal extends Restaurant {

    @Id
    private int mealId;
    private String mealName;
    private float mealAmount;

    public Meal() {
    }

    public Meal(int mealId, String mealName, float mealAmount) {
        this.mealId = mealId;
        this.mealName = mealName;
        this.mealAmount = mealAmount;
    }

    public int getMealId() {
        return mealId;
    }

    public void setMealId(int mealId) {
        this.mealId = mealId;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public float getMealAmount() {
        return mealAmount;
    }

    public void setMealAmount(float mealAmount) {
        this.mealAmount = mealAmount;
    }

    @Override
    public String toString() {
        return "Meal{"
                + "mealId=" + mealId
                + ", mealName='" + mealName + '\''
                + ", mealAmount=" + mealAmount
                + '}';
    }
}
