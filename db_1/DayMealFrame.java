package db_1;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class DayMealFrame extends SuperFrame implements ActionListener {

    JButton bConfirm, bRefresh;
    final JTable table;

    ArrayList<Meal> chosenMeals;
    int selectedRowId, mealId;

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        Object source = e.getSource();

        if (source == miExit) {
            dispose();
        } else if (source == miCook) {
            CookLogin cookLogin = new CookLogin();
        } else if (source == miAssistant) {
            AssistantLogin assistantLogin = new AssistantLogin();
        } else if (source == miAdmin) {
            AdminLogin adminLogin = new AdminLogin();
        } else if (source == bConfirm) {
            Cook cook = new Cook();
            cook.makeMeal(mealId);
            DayMealTableModel model = (DayMealTableModel) table.getModel();
            model.updateMeal(mealId);
        } else if (source == bRefresh) {
            dispose();
            DayMealFrame danieDniaFrame = new DayMealFrame(chosenMeals);
        } else if (source == miHelp) {
            AuthorsFrame authorsFrame = new AuthorsFrame();
        }
    }

    public DayMealFrame(ArrayList<Meal> meals) {
        chosenMeals = meals;
        initMenu();

        setPreferredSize(new Dimension(400, 350));
        pack();
        setTitle("Realizacja dań");
        setVisible(true);

        JLabel instr = new JLabel("Wybierz danie:");
        instr.setFont(new Font("Sans-serif", Font.BOLD, 13));
        instr.setSize(100, 30);
        instr.setLocation(10, 110);
        add(instr);

        bConfirm = new JButton("Wykonaj");
        bConfirm.setLocation(120, 110);
        bConfirm.setSize(100, 30);
        add(bConfirm);
        bConfirm.addActionListener(this);

        bRefresh = new JButton("Odswiez");
        bRefresh.setLocation(230, 110);
        bRefresh.setSize(100, 30);
        add(bRefresh);
        bRefresh.addActionListener(this);

        table = new JTable(new DayMealTableModel(chosenMeals));
        JScrollPane scrollPane = new JScrollPane(table);
        table.setCellSelectionEnabled(false);
        table.setColumnSelectionAllowed(false);
        table.setRowSelectionAllowed(true);
        table.setFillsViewportHeight(false);
        table.setAutoCreateRowSorter(true);
        setWidthAsPercentages(table, 0.05, 0.80, 0.15);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                selectedRowId = table.getSelectedRow();
                if (selectedRowId < 0) {
                    return;
                }
                mealId = Integer.parseInt(table.getValueAt(selectedRowId, 0).toString());
            }
        });
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        add(scrollPane, BorderLayout.CENTER);

    }

    private static void setWidthAsPercentages(JTable table,
            double... percentages) {
        final double factor = 10000;

        TableColumnModel model = table.getColumnModel();
        for (int columnIndex = 0; columnIndex < percentages.length; columnIndex++) {
            TableColumn column = model.getColumn(columnIndex);
            column.setPreferredWidth((int) (percentages[columnIndex] * factor));
        }
    }

}
