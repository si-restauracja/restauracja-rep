package db_1;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;

public class MealTableModel extends AbstractTableModel {

    String[] columnNames = new String[]{"ID", "Nazwa", "Ilość", "Wybór"};
    ArrayList<Meal> mealList;
    ArrayList<Boolean> boolList;
    CookFrame frame;

    public MealTableModel(CookFrame frame) {
        super();
        this.frame = frame;
        Connection connection = new Connection();
        connection.createSession();
        connection.getRecords();
        mealList = new ArrayList<Meal>(connection.mealList);
        boolList = new ArrayList<Boolean>(Collections.nCopies(mealList.size(), Boolean.FALSE));
    }

    @Override
    public int getRowCount() {
        return mealList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnNames[columnIndex];
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 3) {
            return boolList.get(rowIndex);
        }
        Meal meal = mealList.get(rowIndex);
        Object[] values = new Object[]{meal.getMealId(), meal.getMealName(), meal.getMealAmount()};
        return values[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int colIndex) {
        return colIndex == 3;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int colIndex) {
        if (colIndex == 3) {
            Boolean val = Boolean.parseBoolean(value.toString());
            boolList.set(rowIndex, val);
            if (val) {
                frame.boolCount++;
                frame.checkedMeals.add(mealList.get(rowIndex));
            } else {
                frame.boolCount--;
                frame.checkedMeals.remove(mealList.get(rowIndex));
            }
        }
    }

}
