package db_1;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

public class ConnectionTest {

	@Test
	public void testConnection() {
		Connection testConn = new Connection();
	}

	@Test
	public void testCreateSession() {
		Connection testConn = new Connection();
		testConn.createSession();
		Session s = testConn.sf.openSession();
	}
	
	@Test(expected = ExceptionInInitializerError.class)
	public void testForExceptions1() {
		Connection testConn = new Connection();
		
	}

	@Test
	public void testGetRecords() {
		Connection testConn = new Connection();
		testConn.createSession();
		Session s = testConn.sf.openSession();
		testConn.getRecords();
	}

	@Test
	public void testUpdateRecord() {
		Connection testConn = new Connection();
		testConn.createSession();
		Session s = testConn.sf.openSession();
		Transaction t = s.getTransaction();
		t.begin();
		Meal meal = new Meal(10,"Ziemniaki",2);
		testConn.updateRecord(meal);
		t.commit();
	}

}
