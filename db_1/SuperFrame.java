/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db_1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Arek
 */
public class SuperFrame extends JFrame implements ActionListener {

    JMenuBar menuBar;
    JMenu mOptions, mChooseActor, mHelp;
    JMenuItem miExit, miCook, miAssistant, miAdmin, miHelp;

    public void initMenu() {
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        mOptions = new JMenu("Opcje");
        menuBar.add(mOptions);

        mChooseActor = new JMenu("Zaloguj jako");
        mOptions.add(mChooseActor);

        mHelp = new JMenu("Pomoc");
        menuBar.add(mHelp);

        miCook = new JMenuItem("Kucharz");
        miAssistant = new JMenuItem("Pomocnik");
        miAdmin = new JMenuItem("Administrator");
        miCook.addActionListener(this);
        miAssistant.addActionListener(this);
        miAdmin.addActionListener(this);
        mChooseActor.add(miAssistant);
        mChooseActor.add(miCook);
        mChooseActor.add(miAdmin);

        miHelp = new JMenuItem("Pomoc");
        miHelp.addActionListener(this);
        mHelp.add(miHelp);

        mOptions.addSeparator();
        miExit = new JMenuItem("Wyjście");
        mOptions.add(miExit);
        miExit.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

    }
}
