package db_1;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Arek on 2014-11-09.
 */
@Entity(name = "assistants")
public class Assistants {

    @Id
    private int assistantId;
    private String assistantName;
    private String assistantPassword;

    public Assistants() {
    }

    public Assistants(int assistantId, String assistantName, String assistantPassword) {
        this.assistantId = assistantId;
        this.assistantName = assistantName;
        this.assistantPassword = assistantPassword;
    }

    public int getAssistantId() {
        return assistantId;
    }

    public void setAssistantId(int assistantId) {
        this.assistantId = assistantId;
    }

    public String getAssistantName() {
        return assistantName;
    }

    public void setAssistantName(String assistantName) {
        this.assistantName = assistantName;
    }

    public String getAssistantPassword() {
        return assistantPassword;
    }

    public void setAssistantPassword(String assistantPassword) {
        this.assistantPassword = assistantPassword;
    }

    @Override
    public String toString() {
        return "Assistants{" + "assistantId=" + assistantId + ", assistantName=" + assistantName + ", assistantPassword=" + assistantPassword + '}';
    }

}
