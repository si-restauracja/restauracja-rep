/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db_1;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Arek
 */
public class AuthorsFrame extends JFrame {

    JLabel lbAuthors;

    public AuthorsFrame() {
        setLayout(null);
        setTitle("O Autorach");
        setSize(400, 300);
        setLocation(200, 200);
        setVisible(true);

        lbAuthors = new JLabel("<html>System do zarządzania restauracją, wykonany na zajęcia z Systemów Informatycznych. <br><br>Autorzy projektu: <br><br>Arkadiusz Labudda <br>"
                + "Paweł Misiak <br>Przemysław Tusiński <br>Dominik Bujas<br><br>Kraków 2014 </html>");
        lbAuthors.setFont(new Font("Sas-serif", Font.BOLD, 13));
        lbAuthors.setSize(340, 200);
        lbAuthors.setLocation(10, 10);
        add(lbAuthors);

    }

}
