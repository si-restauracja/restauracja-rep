package db_1;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.io.File;
//import java.io.IOException;
import java.util.ArrayList;

//import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class CookFrame extends SuperFrame implements ActionListener {

    JTable table;
    JButton bConfirm, bRefresh;
    int boolCount = 0;
    ArrayList<Meal> checkedMeals = new ArrayList<Meal>();

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == miExit) {
            dispose();
        } else if (source == miCook) {
            CookLogin cookLogin = new CookLogin();
        } else if (source == miAssistant) {
            AssistantLogin assistantLogin = new AssistantLogin();
        } else if (source == miAdmin) {
            AdminLogin adminLogin = new AdminLogin();
        } else if (source == bRefresh) {
            dispose();
            CookFrame kucharzFrame = new CookFrame();
            kucharzFrame.pack();
            kucharzFrame.setTitle("Kucharz");
            kucharzFrame.setVisible(true);

        } else if (source == bConfirm) {
            if (boolCount == 5) {
                DayMealFrame danieDniaFrame = new DayMealFrame(checkedMeals);
//                try {
//                    danieDniaFrame.setIconImage(ImageIO.read(new File("res/cook.png")));
//                } catch (IOException e1) {
//                    // TODO Auto-generated catch block
//                    e1.printStackTrace();
//                }
            } else {
                JOptionPane.showMessageDialog(this, "Musisz zaznaczyć dokładnie 5 dań");
            }
        } else if (source == miHelp) {
            AuthorsFrame authorsFrame = new AuthorsFrame();
        }
    }

    public CookFrame() {

        table = new JTable(new MealTableModel(this));

        int rowHeight = table.getRowHeight();
        int rowCount = table.getRowCount();

        setLocation(400, 0);
        setPreferredSize(new Dimension(400, rowHeight * rowCount + 150));
        pack();
        setTitle("Kucharz");
        setVisible(true);
        setLocation(400, 0);

        //po wejsciu do tego widoku nalezy od razu obliczyc ilosc dan
        Cook cook = new Cook();
        cook.calculateMealQuantity();
        initMenu();

        bConfirm = new JButton("Wybierz");
        bConfirm.setLocation(160, rowHeight * rowCount + 30);
        bConfirm.setSize(100, 30);
        add(bConfirm);
        bConfirm.addActionListener(this);

        bRefresh = new JButton("Odśwież");
        bRefresh.setLocation(270, rowHeight * rowCount + 30);
        bRefresh.setSize(100, 30);
        add(bRefresh);
        bRefresh.addActionListener(this);

        JLabel instr = new JLabel("Wybierz 5 Dań Dnia");
        instr.setFont(new Font("Sans-serif", Font.BOLD, 13));
        instr.setSize(150, 30);
        instr.setLocation(10, rowHeight * rowCount + 30);
        add(instr);

        table.setCellSelectionEnabled(false);
        table.setColumnSelectionAllowed(false);
        table.setRowSelectionAllowed(false);
        JScrollPane scrollPane = new JScrollPane(table);
        //table.setFillsViewportHeight(true);
        table.setAutoCreateRowSorter(true);
        setWidthAsPercentages(table, 0.05, 0.70, 0.20, 0.05);
        add(scrollPane, BorderLayout.CENTER);

    }

    private static void setWidthAsPercentages(JTable table,
            double... percentages) {
        final double factor = 10000;

        TableColumnModel model = table.getColumnModel();
        for (int columnIndex = 0; columnIndex < percentages.length; columnIndex++) {
            TableColumn column = model.getColumn(columnIndex);
            column.setPreferredWidth((int) (percentages[columnIndex] * factor));
        }
    }
}
