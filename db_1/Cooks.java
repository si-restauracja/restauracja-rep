package db_1;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Arek on 2014-11-09.
 */
@Entity(name = "cooks")
public class Cooks extends Restaurant {

    @Id
    private int cookId;
    private String cookName;
    private String cookPassword;

    public Cooks() {
    }

    public Cooks(int cookId, String cookName, String cookPassword) {
        this.cookId = cookId;
        this.cookName = cookName;
        this.cookPassword = cookPassword;
    }

    public int getCookId() {
        return cookId;
    }

    public String getCookName() {
        return cookName;
    }

    public String getCookPassword() {
        return cookPassword;
    }

    public void setCookId(int cookId) {
        this.cookId = cookId;
    }

    public void setCookName(String cookName) {
        this.cookName = cookName;
    }

    public void setCookPassword(String cookPassword) {
        this.cookPassword = cookPassword;
    }

    @Override
    public String toString() {
        return "Cooks{" + "cookId=" + cookId + ", cookName=" + cookName + ", cookPassword=" + cookPassword + '}';
    }

}
