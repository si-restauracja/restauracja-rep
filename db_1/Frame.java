package db_1;
/*
 * Created by Dominik Bujas on 2014-11-10
 * 		Klasa o nazwie Frame odpowiedzialna za wygl�d okienkowy aplikacji  
 * 
 */

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Frame extends SuperFrame implements ActionListener {

    JButton bCook, bAssistant;

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == miExit) {
            dispose();
        } else if (source == miCook) {
            CookLogin cookLogin = new CookLogin();
        } else if (source == miAssistant) {
            AssistantLogin assistantLogin = new AssistantLogin();
        } else if (source == miAdmin) {
            AdminLogin adminLogin = new AdminLogin();
        } else if (source == bCook) {
            CookLogin cookLogin = new CookLogin();
        } else if (source == bAssistant) {
            AssistantLogin assistantLogin = new AssistantLogin();
            //ikona okna pomocnika
//            try {
//                pomocnikFrame.setIconImage(ImageIO.read(new File("res/help.png")));
//            } catch (IOException e1) {
//                // TODO Auto-generated catch block
//                e1.printStackTrace();
//            }
        } else if (source == miHelp) {
            AuthorsFrame authorsFrame = new AuthorsFrame();
        }
    }

    public static void main(String ags[]) {
        Frame frame = new Frame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Manager Restauracji");
        frame.setVisible(true);
        //ikona okna restauracji
//        try {
//
//            frame.setIconImage(ImageIO.read(new File("restaurant.png")));
//        } catch (IOException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
    }

    public Frame() {
        setLayout(null);
        setSize(700, 600);
        initMenu();

        ImageIcon img = createImageIcon("bg.png");
        JLabel background = new JLabel(img);
        background.setLocation(120, 250);
        background.setSize(384, 230);
        background.setVisible(true);
        add(background);

        bCook = new JButton("Kucharz");
        bAssistant = new JButton("Pomocnik");
        bCook.setLocation(100, 150);
        bAssistant.setLocation(400, 150);
        bCook.setSize(200, 100);
        bAssistant.setSize(200, 100);
        add(bCook);
        add(bAssistant);
        bCook.addActionListener(this);
        bAssistant.addActionListener(this);

        JLabel lEkranPowitalny = new JLabel("WITAMY W APLIKACJI MANAGER RESTAURACJI", JLabel.CENTER);
        JLabel lWybierzTryb = new JLabel("Wybierz Tryb", JLabel.CENTER);
        lEkranPowitalny.setFont(new Font("Sans-serif", Font.BOLD, 18));
        lEkranPowitalny.setSize(500, 100);
        lEkranPowitalny.setLocation(100, 0);
        lWybierzTryb.setFont(new Font("Sans-serif", Font.BOLD, 16));
        lWybierzTryb.setSize(500, 200);
        lWybierzTryb.setLocation(100, 0);
        add(lEkranPowitalny);
        add(lWybierzTryb);

    }

    protected ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

}
