package db_1;

import java.util.Iterator;

import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Cook{

    Food food;
    Meal meal;
    Recipe recipe;

    public void calculateMealQuantity() {
        Connection ob = new Connection();
        Food food = new Food();
        Meal meal = new Meal();
        Recipe recipe = new Recipe();
        ob.createSession();
        ob.getRecords();
        Iterator<Meal> mealIterator = ob.mealList.iterator();
        Iterator<Recipe> recipeIterator = ob.recipeList.iterator();
        Iterator<Food> foodIterator = ob.foodList.iterator();
        float mx;
        while (mealIterator.hasNext()) {
            mx = 9999;
            meal = mealIterator.next();
            //	System.out.println("     "+meal.toString());
            recipeIterator = ob.recipeList.iterator();
            while (recipeIterator.hasNext()) {
                foodIterator = ob.foodList.iterator();
                recipe = recipeIterator.next();
                //  System.out.println(recipeIterator.next().toString());
                if (recipe.getMealId() == meal.getMealId()) {
                    while (foodIterator.hasNext()) {
                        food = foodIterator.next();
                        if (food.getFoodId() == recipe.getFoodId()) {
                            // 		   System.out.print(food.toString()+recipe.toString());
                            mx = Math.min(mx, food.getFoodAmount() / recipe.getFoodAmount());
                            // 		   System.out.println(" "+mx);
                        }
                    }
                }
            }
            meal.setMealAmount((int) mx);
            System.out.println(meal.toString());
            ob.updateRecord(meal);
        }
        StandardServiceRegistryBuilder.destroy(ob.serviceRegistry);
    }

    public void makeMeal(int mealId) {
        Connection ob = new Connection();
        Food food = new Food();
        Meal meal = new Meal();
        Recipe recipe = new Recipe();
        ob.createSession();
        ob.getRecords();
        Iterator<Meal> mealIterator = ob.mealList.iterator();
        Iterator<Recipe> recipeIterator = ob.recipeList.iterator();
        Iterator<Food> foodIterator = ob.foodList.iterator();
        while (mealIterator.hasNext()) {
            meal = mealIterator.next();
            //	System.out.println("     "+meal.toString());
            recipeIterator = ob.recipeList.iterator();
            if (mealId == meal.getMealId()) {
                while (recipeIterator.hasNext()) {
                    foodIterator = ob.foodList.iterator();
                    recipe = recipeIterator.next();
                    //  System.out.println(recipeIterator.next().toString());
                    if (recipe.getMealId() == meal.getMealId()) {
                        while (foodIterator.hasNext()) {
                            food = foodIterator.next();
                            if (food.getFoodId() == recipe.getFoodId()) {
                                food.setFoodAmount(food.getFoodAmount() - recipe.getFoodAmount());
                   // 		   System.out.print(food.toString()+recipe.toString());
                                //   System.out.println(food.toString()+" "+recipe.toString());
                                ob.updateRecord(food);
                            }
                        }
                    }
                }
            }

        }
        StandardServiceRegistryBuilder.destroy(ob.serviceRegistry);
        this.calculateMealQuantity();
    }

}
