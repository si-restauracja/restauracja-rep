package db_1;

import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by Arek on 2014-11-10.
 */
public class Connection {

    protected List<Food> foodList;
    protected List<Meal> mealList;
    protected List<Recipe> recipeList;
    protected List<Cooks> cooksList;
    protected List<Assistants> assistantsList;
    protected List<Admins> adminsList;
    protected static ServiceRegistry serviceRegistry;
    protected SessionFactory sf;

    public Connection() {
    }

    protected void createSession() {
        try {
            this.serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    new Configuration().configure().getProperties()).build();
            sf = new Configuration().configure().buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public void getRecords() {
        Session s = sf.openSession();
        Transaction t = s.getTransaction();
        t = null;
        try {
            t = s.beginTransaction();

            foodList = s.createQuery("FROM food").list();
            for (Iterator iterator
                    = foodList.iterator(); iterator.hasNext();) {
                Food food = (Food) iterator.next();
            }

            mealList = s.createQuery("FROM meal").list();
            for (Iterator iterator
                    = mealList.iterator(); iterator.hasNext();) {
                Meal meal = (Meal) iterator.next();
            }

            recipeList = s.createQuery("FROM recipe").list();
            for (Iterator iterator
                    = recipeList.iterator(); iterator.hasNext();) {
                Recipe recipe = (Recipe) iterator.next();
            }

            cooksList = s.createQuery("FROM cooks").list();
            for (Iterator iterator
                    = cooksList.iterator(); iterator.hasNext();) {
                Cooks cooks = (Cooks) iterator.next();
            }

            adminsList = s.createQuery("FROM admins").list();
            for (Iterator iterator
                    = adminsList.iterator(); iterator.hasNext();) {
                Admins admins = (Admins) iterator.next();
            }

            assistantsList = s.createQuery("FROM assistants").list();
            for (Iterator iterator
                    = assistantsList.iterator(); iterator.hasNext();) {
                Assistants assistants = (Assistants) iterator.next();
            }

            t.commit();
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            e.printStackTrace();
        } finally {
            s.close();
        }
    }

    protected void updateRecord(Restaurant r) {
        Session s = sf.openSession();
        Transaction t = s.getTransaction();
        t = null;
        Integer foodId = null;
        Integer mealId = null;
        Integer recipeId = null;
        try {
            t = s.beginTransaction();
            s.saveOrUpdate(r);
            t.commit();
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            e.printStackTrace();
        } finally {
            s.close();
        }
    }

}
