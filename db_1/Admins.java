/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db_1;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Arek
 */
@Entity(name = "admins")
public class Admins {

    @Id
    private int adminId;
    private String adminName;
    private String adminPassword;

    public Admins() {
    }

    public Admins(int adminId, String adminName, String adminPassword) {
        this.adminId = adminId;
        this.adminName = adminName;
        this.adminPassword = adminPassword;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    @Override
    public String toString() {
        return "Admins{" + "adminId=" + adminId + ", adminName=" + adminName + ", adminPassword=" + adminPassword + '}';
    }

}
