package db_1;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class FoodTableModel extends AbstractTableModel {

    Connection connection = new Connection();
    ArrayList<Food> foodList;

    public FoodTableModel() {
        super();
        connection.createSession();
        connection.getRecords();
        foodList = new ArrayList<Food>(connection.foodList);
    }

    @Override
    public int getRowCount() {
        return foodList.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String[] names = new String[]{"ID", "Nazwa", "Ilość"};
        return names[columnIndex];
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Food food = foodList.get(rowIndex);
        Object[] values = new Object[]{food.getFoodId(), food.getFoodName(), food.getFoodAmount()};
        return values[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (colIndex == 2) {
            return true;
        }
        return false;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int colIndex) {
        foodList.get(rowIndex).setFoodAmount(Float.parseFloat(value.toString()));
        connection.updateRecord(foodList.get(rowIndex));
    }
}
