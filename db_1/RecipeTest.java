package db_1;

import static org.junit.Assert.*;

import org.junit.Test;

public class RecipeTest {

	@Test
	public void testRecipe() {
		Recipe test = new Recipe();
	}

	@Test
	public void testRecipeIntIntIntFloat() {
		Recipe test = new Recipe(4,4,4,2.5f);
	}

	@Test
	public void testGetRecipeId() {
		Recipe test = new Recipe(4,4,4,2.5f);
		test.getRecipeId();
	}

	@Test
	public void testSetRecipeId() {
		Recipe test = new Recipe();
		test.setRecipeId(5);
	}

	@Test
	public void testGetMealId() {
		Recipe test = new Recipe(4,4,4,2.5f);
		test.getMealId();
	}

	@Test
	public void testSetMealId() {
		Recipe test = new Recipe();
		test.setMealId(10);
	}

	@Test
	public void testGetFoodId() {
		Recipe test = new Recipe(4,4,4,2.5f);
		test.getFoodId();
	}

	@Test
	public void testSetFoodId() {
		Recipe test = new Recipe();
		test.setFoodId(5);
	}

	@Test
	public void testGetFoodAmount() {
		Recipe test = new Recipe(4,4,4,2.5f);
		test.getFoodAmount();
	}

	@Test
	public void testSetFoodAmount() {
		Recipe test = new Recipe();
		test.setFoodAmount(12);
	}

	@Test
	public void testToString() {
		Recipe test = new Recipe(4,4,4,2.5f);
		test.toString();
	}
}