package db_1;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class KucharzFrame extends JFrame implements ActionListener {
	JButton okej;
	JMenuBar menuBar; //G��wny pasek Menu
	JMenu mPlik, mEdycja, mOpcje, mPomoc; //PodMenu
	JMenuItem miWyborAktora, miOtworz, miZapisz, miWyjscie, miCofnij, miPrzywroc, miUstawienia; //Opcje w Menu(rozwijane)
    int boolCount = 0;
    ArrayList<Meal> checkedMeals = new ArrayList<Meal>();
	
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if(source == miWyjscie || source == miWyborAktora)
			dispose();
		else if(source == okej)	{
            if(boolCount == 5) {
                DanieDniaFrame danieDniaFrame = new DanieDniaFrame(checkedMeals);
                danieDniaFrame.setPreferredSize(new Dimension(400, 350));
                danieDniaFrame.pack();
                //pomocnikFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);//Automatyczna maksymalizacjia okna
                danieDniaFrame.setTitle("Kucharzu realizuj dania");//Nazwa na pasku
                danieDniaFrame.setVisible(true);//ustawiamy widzialno�� g��wnej aplikacji
                try {
                    danieDniaFrame.setIconImage(ImageIO.read(new File("res/cook.png")));
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
            else
                JOptionPane.showMessageDialog(this, "Musisz zaznaczy� dok�adnie 5 da�");
		}
	}

	
	public KucharzFrame(){//po wejsciu do tego widoku nalezy od razu obliczyc ilosc dan
		Cook cook=new Cook();
		cook.calculateMealQuantity();
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		//stworzenie g��wnych bar�w menu
		mPlik = new JMenu("Plik");
		mEdycja = new JMenu("Edycja");
		mOpcje = new JMenu("Opcje");
		mPomoc = new JMenu("Pomoc");
		//dodanie menu do g��wnego paska
		menuBar.add(mPlik);
		menuBar.add(mEdycja);
		menuBar.add(mOpcje);
		menuBar.add(mPomoc);
		//stworzenie podmenu
		miOtworz = new JMenuItem("Otwórz");
		miZapisz = new JMenuItem("Zapisz");
		miWyborAktora = new JMenuItem("Wyb�r aktora");
		miWyjscie = new JMenuItem("Wyj�cie");
		miCofnij = new JMenuItem("Cofnij");
		miPrzywroc = new JMenuItem("Przywr��");
		miUstawienia = new JMenuItem("Ustawienia");
		//dodanie podmenu do menu
		mPlik.add(miOtworz);
		mPlik.add(miZapisz);
		mPlik.add(miWyborAktora);
		mPlik.addSeparator();
		mPlik.add(miWyjscie);
		mEdycja.add(miCofnij);
		mEdycja.add(miPrzywroc);
		mOpcje.add(miUstawienia);
		//actionListenery do podmenu
		miOtworz.addActionListener(this);
		miZapisz.addActionListener(this);
		miWyborAktora.addActionListener(this);
		miWyjscie.addActionListener(this);
		miCofnij.addActionListener(this);
		miPrzywroc.addActionListener(this);
		miUstawienia.addActionListener(this);

		okej = new JButton("OK");
                okej.setLocation(50, 250);
		okej.setSize(60, 30);
		add(okej);
		okej.addActionListener(this);
		
		JLabel instr = new JLabel("Wybierz 5 Dań Dnia");
        instr.setFont(new Font("Sans-serif", Font.BOLD, 13));
        instr.setSize(500, 100);
        instr.setLocation(10,150);
        add(instr);

		JTable table = new JTable(new MealTableModel(this));
        table.setCellSelectionEnabled(false);
        table.setColumnSelectionAllowed(false);
        table.setRowSelectionAllowed(false);
        JScrollPane scrollPane = new JScrollPane(table);
        //table.setFillsViewportHeight(true);
        table.setAutoCreateRowSorter(true);
        setWidthAsPercentages(table, 0.05, 0.70, 0.20, 0.05);
        add(scrollPane, BorderLayout.CENTER);
        
        

	}

	private static void setWidthAsPercentages(JTable table,
	        double... percentages) {
	    final double factor = 10000;
	 
	    TableColumnModel model = table.getColumnModel();
	    for (int columnIndex = 0; columnIndex < percentages.length; columnIndex++) {
	        TableColumn column = model.getColumn(columnIndex);
	        column.setPreferredWidth((int) (percentages[columnIndex] * factor));
	    }
	}
}


