package db_1;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Arek on 2014-11-09.
 */
@Entity(name = "food")
public class Food extends Restaurant {

    @Id
    private int foodId;
    private String foodName;
    private float foodAmount;

    public Food() {
    }

    public Food(int foodId, String foodName, float foodAmount) {
        this.foodId = foodId;
        this.foodName = foodName;
        this.foodAmount = foodAmount;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public float getFoodAmount() {
        return foodAmount;
    }

    public void setFoodAmount(float foodAmount) {
        this.foodAmount = foodAmount;
    }

    @Override
    public String toString() {
        return "Food{"
                + "foodId=" + foodId
                + ", foodName='" + foodName + '\''
                + ", foodAmount=" + foodAmount
                + '}';
    }
}
