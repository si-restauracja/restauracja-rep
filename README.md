# **README** #

## **Konfiguracja bazy danych** ##

Baza danych użyta w systemie jest zarządzana przez program PostgreSQL. System wymaga utworzenia lokalnej wersji bazy restaurant_db, którą tworzymy następująco:

1. Na serwerze tworzymy nową(pustą) bazę danych o nazwie "restaurant_db".
2. Klikamy na bazę PPM i wybieramy opcję "przywróć", następnie wskazujemy lokalizację pliku "restaurant_db.backup", którego aktualna wersja znajduje się w repozytorium.
3. Zatwierdzamy operację.

## **Wymagane biblioteki** ##

System korzysta z zewnętrznych bibliotek do obsługi połączenia z bazą danych. Wymagane biblioteki:

1. hibernate 4.3.7 http://hibernate.org/orm/downloads/ 
2. JDBC4 http://jdbc.postgresql.org/download.html

Do projektu musimy dodać wszystkie pliki .jar. W paczce hibernate'a, wymagane pliki znajdują się w katalogu required.

## **Konfiguracja Hibernate** ##

Do naszego projektu musimy dołączyć plik hibernate.cfg.xml znajdujący się w repozytorium. Należy podać w nim:
1. nazwę użytkownika (connection.username)
2. hasło (connection.password)
3. adres i port (connection.url)

Domyślnie:
1. postgres
2. postgres
3. jdbc:postgresql://localhost:5432/
